#ifndef SensorUtils
#define SensorUtils


#include <Adafruit_INA219.h>

Adafruit_INA219 sensor219;

void setupSensor() {
  sensor219.begin();
  sensor219.setCalibration_32V_1A();
}  

float getCurrent() {
  float shunt_voltage = 0;
  
  for (int i = 0; i < CURRENT_SENSOR_READS_PER_MEASURE; i++) {
      shunt_voltage += sensor219.getShuntVoltage_mV();
  }

  shunt_voltage = shunt_voltage / CURRENT_SENSOR_READS_PER_MEASURE;

  if (shunt_voltage < 0) {
    shunt_voltage = 0;
  }

  return shunt_voltage / SHUNT_MAX_VOLTAGE * SHUNT_MAX_CURRENT;
}

float getVoltage(float current) {
  float bus_voltage = 0;

  for (int i = 0; i < VOLTAGE_SENSOR_READS_PER_MEASURE; i++) {
    bus_voltage += sensor219.getBusVoltage_V();
  }

  bus_voltage = bus_voltage / VOLTAGE_SENSOR_READS_PER_MEASURE - BATTERY_INTERNAL_RESISTANCE * current;

  return bus_voltage;
}

#endif
