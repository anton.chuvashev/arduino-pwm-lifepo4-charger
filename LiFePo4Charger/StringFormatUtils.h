#ifndef StringFormatUtils
#define StringFormatUtils


String formatFloat(float f, int fractional_digits) {

  int multiplier = pow(10, fractional_digits);
  float abs_f    = abs(f);
  
  uint32_t int_part = floor(abs_f);               // 12.34 -> 12
  uint32_t tmp_int  = floor(abs_f) * multiplier;  // 12.34 -> 1200
  uint32_t tmp_full = round(abs_f * multiplier);  // 12.34 -> 1234

  uint32_t fractional_part = abs(tmp_full - tmp_int); // 12.34 -> 34

  String ip;
  String fp = String(fractional_part);

  // 13.999 -> ip=13 fp=100
  if (fp.length() > fractional_digits) {
    ip = String(int_part + 1);
    fp = fp.substring(1,fractional_digits);
  }
  else {
    ip = String(int_part);
  }

  if (f < 0) {
    ip = "-" + ip;
  }

  if (ip.length() < 2) {
    ip = ' ' + ip;
  };

  int fract_zeroes = fractional_digits - fp.length();

  for (int i = 0; i < fract_zeroes; i++) {
    fp = '0' + fp;
  }

  return ip + '.' + fp;
}

String formatInt(float f, int l) {
  uint32_t int_part = uint32_t(f);

  String ip = String(int_part);

  while (ip.length() <  l) {
    ip = ' ' + ip;
  };

  return ip;
}

String formatStr(String s, unsigned int l, String direction) {
  if (direction == ">") {
    while (s.length() < l) {
      s = ' ' + s;
    };
  }
  else if (direction == "<") {
    while (s.length() < l) {
      s = s + ' ';
    };
  }

  return s;
}

String fillStr(String s, unsigned int full_length, unsigned int length) {
  String result = "";

  for (int i = 0; i < full_length; i++) {
    if (i < length) {
      result = result + s;
    }
    else {
      result = result + " ";
    }
  }

  return result;
}

#endif
