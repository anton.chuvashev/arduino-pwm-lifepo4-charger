#ifndef BatteryUtils
#define BatteryUtils


int calculateCharge(float voltage) {
  if (voltage >= 14.37) {
    return 100;
  }
  else if (voltage > 14.33) {
    return 99;
  }
  else if (voltage > 14.30) {
    return 98;
  }
  else if (voltage > 14.25) {
    return 97;
  }
  else if (voltage > 14.20) {
    return 96;
  }
  else if (voltage > 14.14) {
    return 94;
  }
  else if (voltage > 14.00) {
    return 91;
  }
  else if (voltage > 13.90) {
    return 87;
  }
  else if (voltage > 13.80) {
    return 80;
  }
  else if (voltage > 13.72) {
    return 70;
  }
  else if (voltage > 13.65) {
    return 60;
  }
  else if (voltage > 13.60) {
    return 50;
  }
  else if (voltage > 13.55) {
    return 40;
  }
  else if (voltage > 13.50) {
    return 30;
  }
  else if (voltage > 13.45) {
    return 20;
  }
  else if (voltage > 13.40) {
    return 20;
  }
  else if (voltage > 13.20) {
    return 10;
  }
  else if (voltage > 13.00) {
    return 5;
  }
  else {
    return 0;
  }
}

int calculateStandbyCharge(float voltage) {
  if (voltage >= 13.60) {
    return 100;
  }
  else if (voltage > 13.40) {
    return 99;
  }
  else if (voltage > 13.35) {
    return 95;
  }
  else if (voltage > 13.30) {
    return 90;
  }
  else if (voltage > 13.25) {
    return 80;
  }
  else if (voltage > 13.20) {
    return 70;
  }
  else if (voltage > 13.16) {
    return 60;
  }
  else if (voltage > 13.13) {
    return 50;
  }
  else if (voltage > 13.10) {
    return 40;
  }
  else if (voltage > 13.05) {
    return 35;
  }
  else if (voltage > 13.00) {
    return 30;
  }
  else if (voltage > 12.95) {
    return 25;
  }
  else if (voltage > 12.90) {
    return 20;
  }
  else if (voltage > 12.85) {
    return 18;
  }
  else if (voltage > 12.80) {
    return 17;
  }
  else if (voltage > 12.60) {
    return 15;
  }
  else if (voltage > 12.50) {
    return 14;
  }
  else if (voltage > 12.00) {
    return 10;
  }
  else if (voltage > 11.00) {
    return 5;
  }
  else {
    return 0;
  }
}

float getCurrentMaxCurrent(float voltage)
{
    float bmc = BATTERY_MAX_CURRENT;
  
    // 1.025
    float treshhold_voltage = BATTERY_MAX_VOLTAGE / 1.035;

    if (voltage > treshhold_voltage) {
      float k = (BATTERY_MAX_VOLTAGE-voltage)/(BATTERY_MAX_VOLTAGE-treshhold_voltage);

      if (k > 0) {
        bmc = BATTERY_MAX_CURRENT * k * k * k;
      }
    }

    return bmc;
}

#endif
