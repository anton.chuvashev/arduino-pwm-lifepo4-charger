#include <FastLED.h>

CRGB leds[NUM_LEDS];

void initLED() {
  FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, NUM_LEDS);
  FastLED.setMaxPowerInVoltsAndMilliamps(5, 500);
  FastLED.clear();
  FastLED.show();
}

int hexToInt(char hex) {
    if (hex >= '0' && hex <= '9') {
        return hex - '0';
    }
    else if (hex >= 'A' && hex <= 'F') {
        return hex - 'A' + 10;
    }
    else if (hex >= 'a' && hex <= 'f') {
        return hex - 'a' + 10;
    }

    return 0;
}

CRGB hexStringToCRGB(String hexString) {
    if (hexString.length() != 7 || hexString[0] != '#') {
        return CRGB::Black;
    }

    int r = (hexToInt(hexString[1]) << 4) | hexToInt(hexString[2]);
    int g = (hexToInt(hexString[3]) << 4) | hexToInt(hexString[4]);
    int b = (hexToInt(hexString[5]) << 4) | hexToInt(hexString[6]);

    return CRGB(r, g, b);
}

void setLED(String color) {
  leds[0] = hexStringToCRGB(color);

  FastLED.setBrightness(LED_BRIGHTNESS);
  FastLED.show();
}
