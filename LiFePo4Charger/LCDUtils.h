#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 20, 4);

unsigned long display_last_refresh_time = 0;

void setupLCD() {
  lcd.init();
  lcd.backlight();
}

void clearLCD() {
  lcd.clear();
}

void printLCD(String s, int l, int t) {
  lcd.setCursor(l, t);
  lcd.print(s);
}

bool getLCDRefreshRequired(unsigned long time) {
  if (display_last_refresh_time > time) {
    return true;
  }
  else if (time >= display_last_refresh_time + DISPLAY_REFRESH_TIME) {
    return true;
  }
  else {
    return false;
  }
}

void setLCDLastRefreshTime(unsigned long time) {
  display_last_refresh_time = time;
}