#ifndef PWMUtils
#define PWMUtils


const uint32_t MAX_PWM_VALUE = 8191U;

const int outPin9  = 9;
const int outPin10 = 10;


void initPWM() {
  noInterrupts();

// 4096
//  DDRB  |= _BV(PB1)    | _BV(PB2);    /* set pins as outputs */
//  TCCR1A = _BV(COM1A1) | _BV(COM1B1)  /* non-inverting PWM */
//           | _BV(WGM11);              /* mode 14: fast PWM, TOP=ICR1 */
//  TCCR1B = _BV(WGM13) | _BV(WGM12)
//           | _BV(CS11);               /* prescaler 1 */
//  ICR1 = 0x0fff;                      /* TOP counter value (freeing OCR1A*/

// 65536
TCCR1A = 1 << WGM11 | 1 << COM1A1 | 1 << COM1B1; // set on top, clear OC on compare match
TCCR1B = 1 << CS10  | 1 << WGM12 | 1 << WGM13;   // clk/1, mode 14 fast PWM
ICR1 = MAX_PWM_VALUE;

  interrupts();

  pinMode(outPin9,  OUTPUT);
  pinMode(outPin10, OUTPUT);
}

void setPWM(uint32_t chargePinValue) {
  noInterrupts();
  // OCR1A = chargePinValue; // 9й порт
  OCR1B = chargePinValue;    // 10й порт
  interrupts();
}

#endif
