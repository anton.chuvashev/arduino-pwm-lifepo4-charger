#include "Config.h"
#include "StringFormatUtils.h"
#include "BatteryUtils.h"
#include "PWMUtils.h"
#include "SensorUtils.h"
#include "LEDUtils.h"
#include "LCDUtils.h"


// Console input
float    requiredVoltage  = 0;
float    requiredCurrent  = 0;
float    requiredPinValue = 0;

// Calculated values
uint32_t chargePinValue    = 0;
String   mode              = MODE_CHARGED;
float    currentMaxCurrent = BATTERY_MAX_CURRENT;
int      fanSpeed          = 0;
int      isPausePressed    = 0;

// Sensor values
float    current = 0; // V
float    voltage = 0; // A

// Timer
unsigned long time = millis();


void setup()
{
  setupLCD();
  printLCD("Let the light shine!", 0, 1);

  initPins();

  setupSensor();

  Serial.begin(115200);

  initLED();

  initPWM();

  clearLCD();
}

void loop() {
  time    = millis();

  // Charge process
  current = getCurrent();
  voltage = getVoltage(current);

  updateChargeMode();

  updateCurrentMaxCurrent();

  int delta = getChargePinDelta();

  chargePinValue = getPwmValue(delta);

  setPWM(chargePinValue);

  // MOSFET fan state
  updateMOSFETFanState();

  // Screen and LED
  refreshScreen();
  refreshLed();

  // Debug
  printDebug();
  readSerial();
}

void updateChargeMode() {
  calculateChargeMode();
  processButtons();
  checkMainsSensor();
}

void updateCurrentMaxCurrent() {
  float bmc = getCurrentMaxCurrent(voltage);

  if (mode == MODE_HOLD) {
    currentMaxCurrent = BATTERY_MAX_CURRENT;
  }
  
  if (currentMaxCurrent > bmc) {
    currentMaxCurrent = bmc;
  }
}

void processButtons() {
  if (digitalRead(FORCE_PAUSE_PIN) == HIGH) {
    if(!isPausePressed) {
      if (mode != MODE_PAUSE) {
        mode = MODE_PAUSE;
      }
      else {
        mode = MODE_HOLD;
      }

      isPausePressed = 1;
    }
  }
  else {
    isPausePressed = 0;
  }

  if (digitalRead(FORCE_CHARGE_PIN) == HIGH) {
    if (mode == MODE_CHARGED or mode == MODE_PAUSE) {
      mode = MODE_HOLD;
    }
  }
}

void checkMainsSensor() {
  if (digitalRead(MAINS_DETECTION_PIN) == HIGH and mode == MODE_NO_MAINS) {
    mode = MODE_CHARGED;
    delay(MAINS_ON_DELAY);
  }

  if (digitalRead(MAINS_DETECTION_PIN) == LOW) {
    mode = MODE_NO_MAINS;
  }
}

void updateMOSFETFanState() {
  float requiredFanSpeed = current / BATTERY_MAX_CURRENT * FAN_MOSFET_MAX_PWM;

  if (requiredFanSpeed < FAN_MOSFET_CUT_OFF_PWM) {
    fanSpeed = 0;
  }
  else if (fanSpeed < requiredFanSpeed) {
    fanSpeed += FAN_MOSFET_PWM_DELTA_UP;
  }
  else if (fanSpeed > requiredFanSpeed) {
    fanSpeed -= FAN_MOSFET_PWM_DELTA_DOWN;
  }

  if (fanSpeed > FAN_MOSFET_MAX_PWM) {
    fanSpeed = FAN_MOSFET_MAX_PWM;
  }

  if (fanSpeed < 0) {
    fanSpeed = 0;
  }

  analogWrite(FAN_PIN, fanSpeed);
}

uint32_t getPwmValue(int delta) {
  if (mode == MODE_CHARGED or mode == MODE_PAUSE or mode == MODE_NO_MAINS) {
    chargePinValue = 0;
  }
  else if (mode == MODE_HOLD) {
    chargePinValue = PENDING_CHARGE_PIN_VALUE;
  }
  else {
    chargePinValue = chargePinValue + delta;
  }  

  if (chargePinValue < 0) {
    chargePinValue = 0;
  }

  if (chargePinValue > MAX_PWM_VALUE) {
    chargePinValue = MAX_PWM_VALUE;
  }

  return chargePinValue;
}

int getChargePinDelta() {
  int delta = 0;

  // Manual modes
  if (mode == MODE_REQ_VOLTAGE) {
    delta = (requiredVoltage - voltage) * MANUAL_MODE_CHARGE_PIN_DELTA;
  }
  else if (mode == MODE_REQ_CURRENT) {
    delta = (requiredCurrent - current) * MANUAL_MODE_CHARGE_PIN_DELTA;
  }
  else if (mode == MODE_REQ_PWM) {
    delta = requiredPinValue - chargePinValue;
  }
  // Calculated modes
  else if (mode == MODE_CHARGING) {
    delta = (chargePinValue * currentMaxCurrent / current - chargePinValue) * CHARGE_PIN_DELTA_STEP_FACTOR;

    if (delta > 0) {
      delta = ceil(delta);

      if (delta > MAX_CHARGE_PIN_DELTA) {
        delta = MAX_CHARGE_PIN_DELTA;
      }
    }
    else {
      delta = floor(delta);

      if (delta < -MAX_CHARGE_PIN_DELTA) {
        delta = -MAX_CHARGE_PIN_DELTA;
      }
    }
  }

  return delta;
}

void calculateChargeMode() {
  // Manual
  if (requiredVoltage != 0) {
    mode = MODE_REQ_VOLTAGE;
  }
  else if (requiredCurrent != 0) {
    mode = MODE_REQ_CURRENT;
  }
  else if (requiredPinValue != 0) {
    mode = MODE_REQ_PWM;
  }
  // Calculated
  else if (mode == MODE_HOLD and current > BATTERY_DETECTION_CURRENT) {
    mode = MODE_CHARGING;
  }
  else if (mode == MODE_CHARGED and voltage < BATTERY_MIN_VOLTAGE) {
    mode = MODE_HOLD;
  }
  else if (
    mode == MODE_CHARGING
    and (
      current <= BATTERY_CV_MIN_CURRENT             // Charged
      or voltage >= BATTERY_MAX_VOLTAGE             // Charged
      or currentMaxCurrent < BATTERY_CV_MIN_CURRENT // Disconnected
    )
  ) {
    mode = MODE_CHARGED;
  }
}

void refreshScreen() {
  if (getLCDRefreshRequired(time)) {
    int charge;
    
    if (mode == MODE_CHARGING) {
      charge = calculateCharge(voltage);
    }
    else {
      charge = calculateStandbyCharge(voltage);
    }

    printLCD(formatStr(mode, 9, "<"), 0, 0);
    printLCD(formatInt(charge, 3) + "%", 16, 0);
    printLCD("V=" + formatFloat(voltage, 2) + " (" + formatFloat(BATTERY_MAX_VOLTAGE, 2) + ")", 0, 1);
    printLCD("I=" + formatFloat(current, 2) + " (" + formatFloat(currentMaxCurrent, 1) + "/" + formatFloat(BATTERY_MAX_CURRENT, 1) + ")", 0, 2);
    printLCD("PWM:" + formatInt(chargePinValue, 4), 0, 3);
    printLCD("[" + fillStr("#", 9, ceil(charge*9/100)) + "]", 9, 3);

    setLCDLastRefreshTime(time);
  }
}

void refreshLed() {
  if (mode == MODE_PAUSE or mode == MODE_NO_MAINS) {
    setLED("#FF00FF");
  }
  else if (mode == MODE_HOLD) {
    setLED("#0000FF");
  }
  else if (mode == MODE_CHARGING) {
    if (currentMaxCurrent == BATTERY_MAX_CURRENT) {
      setLED("#FF0000");
    }
    else {
      setLED("#FFB400");
    }
  }
  else if (mode == MODE_CHARGED) {
    setLED("#00FF00");
  }
  else {
    setLED("#000000");
  }
}

void printDebug() {
  Serial.print("I=" + formatFloat(current,2) + " ");
  Serial.print("U=" + formatFloat(voltage,2) + " ");
  Serial.print("Mode=" + mode + " ");
  Serial.print("PWM=" + formatFloat(chargePinValue,2) + " ");
  Serial.println("");
}

void initPins() {
  pinMode(FAN_PIN, OUTPUT);
  analogWrite(FAN_PIN, 0);

  pinMode(FORCE_CHARGE_PIN, INPUT);
  pinMode(FORCE_PAUSE_PIN, INPUT);
  pinMode(MAINS_DETECTION_PIN, INPUT);
}

void readSerial() {
  if (Serial.available() > 0) {
    String incomingByte = Serial.readString();

    String cmd   = incomingByte.substring(0,1);
    String value = incomingByte.substring(1);

    value.trim();
    cmd.toUpperCase();

    if (cmd.toInt()) {
      cmd   = "U";
      value = incomingByte;
    }

    requiredVoltage  = 0;
    requiredCurrent  = 0;
    requiredPinValue = 0;

    if (cmd == "I") {
      requiredCurrent = value.toFloat();
      Serial.println("Set required current to " + value);
    }
    else if (cmd == "U") {
      requiredVoltage = value.toFloat();
      Serial.println("Set required voltage to " + value);
    }
    else if (cmd == "P") {
      requiredPinValue = value.toFloat();
      Serial.println("Set required pin value to " + value);
    }
    else {
      Serial.println("Unknown command " + cmd + " with value: " + value);
      Serial.println("Usage:");
      Serial.println("\tSet voltage:   U14.4");
      Serial.println("\tSet current:   I20");
      Serial.println("\tSet pwm value: P1024");
    }
  }
}
