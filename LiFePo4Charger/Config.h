#ifndef LiFePo4Charger
#define LiFePo4Charger

// Pins
#define LED_PIN 4
#define FAN_PIN 5
#define FORCE_CHARGE_PIN 6
#define FORCE_PAUSE_PIN 7
#define NUM_LEDS 1
#define FORCE_PAUSE_PIN 7
#define MAINS_DETECTION_PIN 8

// Battery
#define BATTERY_MAX_VOLTAGE            14.5 // V
#define BATTERY_MIN_VOLTAGE            13.1 // V
#define BATTERY_MAX_CURRENT              50 // A
#define BATTERY_CV_MIN_CURRENT          0.1 // A
#define BATTERY_DETECTION_CURRENT       0.5 // A
#define BATTERY_INTERNAL_RESISTANCE 0.00405 // Ohm

// Charge PWM
#define PENDING_CHARGE_PIN_VALUE      500
#define MANUAL_MODE_CHARGE_PIN_DELTA   10
#define MAX_CHARGE_PIN_DELTA         2000
#define CHARGE_PIN_DELTA_STEP_FACTOR  0.9

// Fan PWM
#define FAN_MOSFET_MAX_PWM        255
#define FAN_MOSFET_CUT_OFF_PWM     50
#define FAN_MOSFET_PWM_DELTA_UP     5
#define FAN_MOSFET_PWM_DELTA_DOWN   5

// Display
#define DISPLAY_REFRESH_TIME 1000 // ms
#define LED_BRIGHTNESS 64         // 0..255

// Sensor
#define CURRENT_SENSOR_READS_PER_MEASURE 400
#define VOLTAGE_SENSOR_READS_PER_MEASURE 400

// Shunt
#define SHUNT_MAX_VOLTAGE 75  // mV
#define SHUNT_MAX_CURRENT 100 // A

#define MAINS_ON_DELAY 3 //s

// Modes
#define MODE_CHARGED  "Charged"
#define MODE_CHARGING "Charge"
#define MODE_PAUSE    "Pause"
#define MODE_HOLD     "Hold"
#define MODE_NO_MAINS "Off"

#define MODE_REQ_VOLTAGE "Req V"
#define MODE_REQ_CURRENT "Req I"
#define MODE_REQ_PWM     "Req PWM"

#endif
